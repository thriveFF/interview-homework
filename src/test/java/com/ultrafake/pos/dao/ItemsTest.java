package com.ultrafake.pos.dao;

import com.ultrafake.pos.model.Item;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
@ExtendWith(SpringExtension.class)
public class ItemsTest {

    @Autowired
    Items items;

    @Test
    public void getAllItems() throws Exception {
        List<Item> allItems = items.getAllItems();

        assertTrue(allItems.size() > 2);

        BigDecimal sum =
                allItems.stream()
                        .map(Item::getPrice)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

        assertTrue(sum.compareTo(BigDecimal.ZERO) > 0);

        Set<String> itemNames =
                allItems.stream().map(Item::getName).collect(Collectors.toSet());


        assertTrue(allItems.size() == itemNames.size());
    }

    @Test
    public void itemFor() throws Exception {
        // The apostrophe ain't the one on the keyboard (it's ’ not ').
        Item item = items.itemFor("Meat Lover’s Pizza");

        assertNotNull(item);
        assertNotNull(item.getPrice());
        assertTrue(item.getPrice().compareTo(BigDecimal.ZERO) > 0);
    }

}