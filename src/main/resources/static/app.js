document.addEventListener('DOMContentLoaded', function () {
  var fakePosApp = new Vue({
    el: '#orderEntryScreen',
    data() {
      return {
        availableItems: null
      };
    },
    created() {
      var me = this;

      axios.get('/api/items/getAllItems').then(function (response) {
        me.availableItems = response.data;
      });
    },
    methods: {
      onItemSelected(item) {
      }
    }
  });
});
